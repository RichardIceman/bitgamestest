﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BitGamesTest
{
    public class CreateAgent : MonoBehaviour
    {
        public GameObject agent;
        public GameObject obstacle;

        private Player player;

        private Toggle colAvoid;
        private Toggle wander;
        private Dropdown steeringBoids;
        private Dropdown steeringPlayer;
        private Slider velocity;
        private Slider force;
        private Text velInfo;
        private Text forceInfo;

        private LinkedList<Boid> listBoids;
        private LinkedList<Obstacle> listObstacle;

        private MoveType curType = MoveType.Seek;

        private void Awake()
        {
            listBoids = new LinkedList<Boid>();
            listObstacle = new LinkedList<Obstacle>();
            player = GameObject.Find("Player").GetComponent<Player>();
            player.listObstacle = listObstacle;

            steeringBoids = GameObject.Find("Canvas/Dropdown").GetComponent<Dropdown>();
            steeringPlayer = GameObject.Find("Canvas/Dropdown (1)").GetComponent<Dropdown>();
            colAvoid = GameObject.Find("Canvas/Toggle").GetComponent<Toggle>();
            wander = GameObject.Find("Canvas/Toggle (1)").GetComponent<Toggle>();
            velocity = GameObject.Find("Canvas/Slider").GetComponent<Slider>();
            force = GameObject.Find("Canvas/Slider (1)").GetComponent<Slider>();
            velInfo = GameObject.Find("Canvas/Text (6)").GetComponent<Text>();
            forceInfo = GameObject.Find("Canvas/Text (7)").GetComponent<Text>();

            steeringBoids.onValueChanged.AddListener(delegate { DropdownBoidsValueChange(); });
            steeringPlayer.onValueChanged.AddListener(delegate { DropdownPlayerValueChange(); });
            colAvoid.onValueChanged.AddListener(delegate { CollisionAvoidensValueChange(); });
            wander.onValueChanged.AddListener(delegate { WanderValueChange(); });

            force.onValueChanged.AddListener(delegate { ForceValueChange(); });
            velocity.onValueChanged.AddListener(delegate { VelocityValueChange(); });
        }

        void Update()
        {
            //ПКМ бойд
            if (Input.GetMouseButtonDown(1))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Vector3 position = hit.point;
                    position.y = 1;

                    GameObject newAgent = Instantiate(agent, position, Quaternion.identity);

                    Boid boid = newAgent.GetComponent<Boid>();
                    boid.listBoids = listBoids;
                    boid.listObstacle = listObstacle;

                    boid.manager.moveType = curType;
                    boid.manager.isCollisionAvoidens = colAvoid.isOn;
                    boid.manager.isWander = wander.isOn;
                    boid.MaxVelocity = velocity.value;
                    boid.MaxForce = force.value;

                    listBoids.AddLast(boid);
                }
            }

            //СКМ препятствие
            if (Input.GetMouseButtonDown(2))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Vector3 position = hit.point;
                    position.y = 1;

                    GameObject newObs = Instantiate(obstacle, position, Quaternion.identity);

                    Obstacle obs = newObs.GetComponent<Obstacle>();
                    listObstacle.AddLast(obs);

                    player.manager.alg.UpdateGrid(obs);
                }
            }
        }

        private void DropdownPlayerValueChange()
        {
            switch (steeringPlayer.value)
            {
                case 0:
                    player.manager.moveType = MoveType.Arrival;
                    break;
                case 1:
                    player.manager.moveType = MoveType.PathFolow;
                    break;
            }
        }

        private void DropdownBoidsValueChange()
        {
            switch(steeringBoids.value)
            {
                case 0:
                    curType = MoveType.Seek;
                    break;
                case 1:
                    curType = MoveType.Flee;
                    break;
                case 2:
                    curType = MoveType.Arrival;
                    break;
                case 3:
                    curType = MoveType.LeaderFoloving;
                    break;
                case 4:
                    curType = MoveType.Pursuit;
                    break;
                case 5:
                    curType = MoveType.Avade;
                    break;
                case 6:
                    curType = MoveType.Flock;
                    break;
            }

            foreach (Boid boid in listBoids)
                boid.manager.moveType = curType;
        }

        private void CollisionAvoidensValueChange()
        {
            foreach (Boid boid in listBoids)
                boid.manager.isCollisionAvoidens = colAvoid.isOn;
        }

        private void WanderValueChange()
        {
            foreach (Boid boid in listBoids)
                boid.manager.isWander = wander.isOn;
        }

        private void ForceValueChange()
        {
            forceInfo.text = force.value.ToString();
            foreach (Boid boid in listBoids)
                boid.MaxForce = force.value;
        }

        private void VelocityValueChange()
        {
            velInfo.text = velocity.value.ToString();
            foreach (Boid boid in listBoids)
                boid.MaxVelocity = velocity.value;
        }
    }
}
