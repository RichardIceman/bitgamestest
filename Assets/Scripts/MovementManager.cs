﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BitGamesTest
{
    public interface IMove
    {
        IMove target { get; }
        LinkedList<Boid> listBoids { get; }
        LinkedList<Obstacle> listObstacle { get; }
        Vector3 Position { get; }
        float MaxVelocity { get; }
        float MaxForce { get; }
        float MaxSeeAhead { get; }
        float LeaderBehindDist { get; }
        Vector3 Velocity { get; set; }
        float FleeRadius { get; }
        float Radius { get; }
        float VisionRadius { get; }
    }

    public enum MoveType
    {
        Seek,
        Flee,
        Arrival,
        LeaderFoloving,
        Pursuit,
        Avade,
        Flock,
        PathFolow
    }

    public class SteeringManager
    {
        private IMove obj;

        public MoveType moveType;

        public bool isWander { get; set; }
        public bool isCollisionAvoidens { get; set; }

        private float circleDistance = 0.1f;
        private float circleRadius = 0.05f;
        private float wanderAngle = 0;
        private float angleChange = Mathf.PI / 18;
        private float slowingRadius = 9;
        private float sightRadius = 4;
        private float separationForce = 0.2f;
        private float cohesionForce = 0.15f;
        private float alignmentForce = 0.5f;
        private float colAvoideForce = 2f;
        private float radiusSkip = 0.5f;

        private List<Vector3> path;

        public AStar alg;

        public SteeringManager(IMove target, Vector2 sizeMoveField)
        {
            obj = target;
            alg = new AStar(sizeMoveField, 50, 100);
        }

        public void Move(Vector3 position)
        {
            switch (moveType)
            {
                case MoveType.Seek:
                    obj.Velocity += Seek(position);
                    break;
                case MoveType.Flee:
                    obj.Velocity += Flee(position);
                    break;
                case MoveType.Pursuit:
                    obj.Velocity += Pursuit(position);
                    break;
                case MoveType.Avade:
                    obj.Velocity += Avade(position);
                    break;
                case MoveType.Arrival:
                    obj.Velocity += Arrive(position, slowingRadius);
                    break;
                case MoveType.LeaderFoloving:
                    obj.Velocity += leaderFoloving();
                    break;
                case MoveType.Flock:
                    obj.Velocity += Flock();
                    break;
                case MoveType.PathFolow:
                    obj.Velocity += FollowPath();
                    break;
            }

            if (isWander) obj.Velocity += Wander();
            if (isCollisionAvoidens) obj.Velocity += CollisionAvoidens();
         
            obj.Velocity = Truncate(obj.Velocity, obj.MaxVelocity);
        }

        private Vector3 Seek(Vector3 targetPos)
        {
            Vector3 desVelocity = Vector3.Normalize(targetPos - obj.Position) * obj.MaxVelocity;
            desVelocity = Truncate(desVelocity, obj.MaxVelocity);

            Vector3 steering = Truncate(desVelocity - obj.Velocity, obj.MaxForce);

            return steering;
        }

        private Vector3 Flee(Vector3 targetPos)
        {
            Vector3 steering = Vector3.zero;

            if (Vector3.Distance(obj.Position, targetPos) <= obj.FleeRadius)
            {
                Vector3 desVelocity = Vector3.Normalize(obj.Position - targetPos) * obj.MaxVelocity;
                desVelocity = Truncate(desVelocity, obj.MaxVelocity);
                steering = Truncate(desVelocity - obj.Velocity, obj.MaxForce);
                steering.y = 0;
            }

            return steering;
        }

        private Vector3 Pursuit(Vector3 targetPos)
        {
            float T = Vector3.Distance(obj.Position, targetPos) / obj.MaxVelocity;
            targetPos += obj.target.Velocity * T;
            return Seek(targetPos);
        }

        private Vector3 Avade(Vector3 targetPos)
        {
            float T = Vector3.Distance(obj.Position, targetPos) / obj.MaxVelocity;
            targetPos += obj.target.Velocity * T;
            return Flee(targetPos);
        }

        private Vector3 Arrive(Vector3 targetPos, float slowingRadius)
        {
            float lenght = Vector3.Distance(obj.Position, targetPos);

            Vector3 desVelocity = Vector3.Normalize(targetPos - obj.Position) * obj.MaxVelocity;
            if (slowingRadius < lenght)
                desVelocity = Truncate(desVelocity, obj.MaxVelocity);
            else
                desVelocity = Truncate(desVelocity, obj.MaxVelocity) * lenght / slowingRadius;

            Vector3 steering = Truncate(desVelocity - obj.Velocity, obj.MaxForce);

            return steering;
        }

        private Vector3 Wander()
        {
            Vector3 positionCircle = obj.Velocity.normalized * circleDistance;

            Vector3 displacement = new Vector3(0, 0, -1);

            float length = displacement.magnitude;

            displacement.x = Mathf.Cos(wanderAngle) * length;
            displacement.z = Mathf.Sin(wanderAngle) * length;

            displacement *= circleRadius;

            wanderAngle += Random.value * angleChange - angleChange / 2;

            Vector3 wanderForce = positionCircle + displacement;

            return wanderForce;
        }

        private Vector3 CollisionAvoidens()
        {
            Vector3 avoidForce = Vector3.zero;

            Obstacle obstacle = findClosest();

            if (obstacle != null)
            {
                Vector3 seeAhead = obj.Position + obj.Velocity.normalized * obj.MaxSeeAhead;
                Vector3 seeAhead2 = seeAhead / 2;
                Vector3 seeAhead3 = obj.Position + obj.Velocity.normalized * obj.Radius;

                float distance = Vector3.Distance(seeAhead, obstacle.Position);
                float distance2 = Vector3.Distance(seeAhead2, obstacle.Position);
                float distance3 = Vector3.Distance(seeAhead3, obstacle.Position);

                if (distance < obj.Radius + obstacle.Radius)
                {
                    avoidForce = seeAhead - obstacle.Position;
                    avoidForce = avoidForce.normalized * colAvoideForce;
                }
                else if (distance2 < obj.Radius + obstacle.Radius)
                {
                    avoidForce = seeAhead2 - obstacle.Position;
                    avoidForce = avoidForce.normalized * colAvoideForce;
                }
                else if(distance3 < obj.Radius + obstacle.Radius)
                {
                    avoidForce = seeAhead3 - obstacle.Position;
                    avoidForce = avoidForce.normalized * colAvoideForce;
                }
            }

            return avoidForce;
        }

        private Vector3 leaderFoloving()
        {
            Vector3 steering = Vector3.zero;

            Vector3 tv = -obj.target.Velocity;
            tv = obj.target.Position +  tv.normalized * obj.LeaderBehindDist;

            steering = Arrive(tv, slowingRadius);

            Vector3 leaderAhead = obj.target.Position - tv;
            if (Vector3.Distance(leaderAhead, obj.Position) < sightRadius ||
                Vector3.Distance(obj.target.Position, obj.Position) < sightRadius)
            {
                steering += Avade(obj.target.Position);
            }

            steering += Separation();

            return steering;
        }

        private Vector3 Separation()
        {
            Vector3 steering = Vector3.zero;

            Boid me = obj as Boid;

            int count = 0;

            foreach (Boid boid in obj.listBoids)
                if (boid != me && Vector3.Distance(obj.Position, boid.Position) < obj.VisionRadius)
                {
                    Vector3 distance = boid.Position - obj.Position;

                    float k = obj.VisionRadius / distance.magnitude;

                    steering += -distance.normalized * k * 0.02f;
                    count++;
                }

            if (count != 0) steering /= count;

            return steering;
        }

        private Vector3 Flock()
        {
            Vector3 steering = Vector3.zero;

            Vector3 sepForce = Vector3.zero;
            Vector3 avgPosition = Vector3.zero;
            Vector3 avgVelocity = Vector3.zero;

            Boid me = obj as Boid;

            int count = 0;

            foreach (Boid boid in obj.listBoids)
                if (boid != me && Vector3.Distance(obj.Position, boid.Position) < obj.VisionRadius)
                {
                    Vector3 distance = boid.Position - obj.Position;

                    sepForce += -distance.normalized * separationForce;

                    avgPosition += boid.Position;
                    avgVelocity += boid.Velocity;

                    count++;
                }

            if (count != 0)
            {
                //separation
                sepForce /= count;
                steering += sepForce;

                //cohesion
                avgPosition /= count;

                Vector3 cohForce = avgPosition - obj.Position;
                cohForce = cohForce.normalized * cohesionForce;
                steering += cohForce;

                //alignment
                avgVelocity /= count;
                steering += avgVelocity.normalized * alignmentForce;
            }

            return steering;
        }

        public void findPath(Vector3 newPosition)
        {
            path = alg.FindPath(obj.Position, newPosition);
        }

        private Vector3 FollowPath()
        {
            Vector3 steering = Vector3.zero;

            if (path == null) return steering;

            steering = Arrive(path[0], slowingRadius);

            if (path.Count > 1 && Vector3.Distance(obj.Position, path[0]) < radiusSkip) path.RemoveAt(0);

            return steering;
        }

        private Obstacle findClosest()
        {
            Obstacle closest = null;
            float minDistance = obj.MaxSeeAhead;

            foreach (Obstacle obs in obj.listObstacle)
            {   
                float distance = Vector3.Distance(obj.Position, obs.Position);
                if (distance <= minDistance)
                {
                    minDistance = distance;
                    closest = obs;
                }
            }

            return closest;
        }

        private Vector3 Truncate(Vector3 vector, float maxValue)
        {
            if (vector.magnitude > 0)
            {
                float i = maxValue / vector.magnitude;

                if (i > 1) i = 1;

                return vector * i;
            }
            return vector;
        }
    }
}
