﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BitGamesTest
{
    public class Player : MonoBehaviour, IMove
    {
        private Vector3 newPos;

        [SerializeField]
        private float maxVelosity = 1;
        [SerializeField]
        private float maxForce = 1;
        [SerializeField]
        private float maxSeeAhead = 4;
        [SerializeField]
        private float leaderBehindDist = 3;
        [SerializeField]
        private float visionRadius = 10;

        public IMove target { get; set; }
        public LinkedList<Obstacle> listObstacle { get; set; }
        public LinkedList<Boid> listBoids { get; set; }
        public Vector3 Velocity { get; set; }
        public float MaxVelocity { get { return maxVelosity; } }
        public float MaxForce { get { return maxForce; } }
        public float FleeRadius { get; set; }
        public float MaxSeeAhead { get { return maxSeeAhead; } }
        public float LeaderBehindDist { get { return leaderBehindDist; } }
        public Vector3 Position { get { return transform.position; } }
        public float VisionRadius { get { return visionRadius; } }

        public float Radius { get; set; }

        private Vector3 minPosition = Vector3.zero;
        private Vector3 maxPosition = new Vector3(200, 0, 100);

        public SteeringManager manager;

        private void Awake()
        {
            GetComponent<MeshRenderer>().material.color = Color.green;

            Vector2 sizeField = new Vector2(maxPosition.x, maxPosition.z);
        
            manager = new SteeringManager(this, sizeField);
            manager.moveType = MoveType.Arrival;
            newPos = transform.position;

            Radius = GetComponent<MeshFilter>().sharedMesh.bounds.size.x * transform.localScale.x / 2;
        }

        void Update()
        {
            //ЛКМ
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    newPos = hit.point;
                    newPos.y = 1;
                    if (manager.moveType == MoveType.PathFolow) manager.findPath(newPos);
                }
            }


            manager.Move(newPos);
            transform.position += Velocity;

            //выход за края
            if (transform.position.x < minPosition.x)
                transform.position = new Vector3(maxPosition.x, transform.position.y, transform.position.z);
            else if (transform.position.x > maxPosition.x)
                transform.position = new Vector3(minPosition.x, transform.position.y, transform.position.z);

            if (transform.position.z < minPosition.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, maxPosition.z);
            else if (transform.position.z > maxPosition.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, minPosition.z);
        }
    }
}

