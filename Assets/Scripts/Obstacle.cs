﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public Vector3 Position { get { return transform.position; } }
    public float Radius { get; private set; }

    void Awake () {
        GetComponent<MeshRenderer>().material.color = Color.blue;

        Radius = GetComponent<MeshFilter>().sharedMesh.bounds.size.x * transform.localScale.x / 2;
    }
}
