﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BitGamesTest
{

    public class Boid : MonoBehaviour, IMove
    {

        [SerializeField]
        private float maxVelosity = 1;
        [SerializeField]
        private float maxForce = 1;
        [SerializeField]
        private float fleeRadius = 20;
        [SerializeField]
        private float maxSeeAhead = 4;
        [SerializeField]
        private float leaderBehindDist = 4;
        [SerializeField]
        private float visionRadius = 10;

        public IMove target { get; set; }
        public LinkedList<Obstacle> listObstacle { get; set; }
        public LinkedList<Boid> listBoids { get; set; }
        public Vector3 Velocity { get; set; }
        public float MaxVelocity { get { return maxVelosity; } set { maxVelosity = value; } }
        public float MaxForce { get { return maxForce; } set { maxForce = value; } }
        public float FleeRadius { get { return fleeRadius; } }
        public float MaxSeeAhead { get { return maxSeeAhead; } }
        public float LeaderBehindDist { get { return leaderBehindDist; } }
        public Vector3 Position { get { return transform.position; } }
        public float VisionRadius { get { return visionRadius; } }

        public float Radius { get; set; }

        private Vector3 minPosition = Vector3.zero;
        private Vector3 maxPosition = new Vector3(200, 0, 100);

        public SteeringManager manager;

        private void Awake()
        {
            GetComponent<MeshRenderer>().material.color = Color.red;

            target = GameObject.Find("Player").GetComponent<IMove>();

            Vector2 sizeField = new Vector2(maxPosition.x, maxPosition.z);
            manager = new SteeringManager(this, sizeField);

            manager.moveType = MoveType.Seek;
            manager.isWander = false;
            manager.isCollisionAvoidens = false;

            Radius = GetComponent<MeshFilter>().sharedMesh.bounds.size.x * transform.localScale.x / 2;
        }

        void FixedUpdate()
        {
            manager.Move(target.Position);
            transform.Translate(Velocity);

            //выход за края
            if (transform.position.x < minPosition.x)
                transform.position = new Vector3(maxPosition.x, transform.position.y, transform.position.z);
            else if (transform.position.x > maxPosition.x)
                transform.position = new Vector3(minPosition.x, transform.position.y, transform.position.z);

            if (transform.position.z < minPosition.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, maxPosition.z);
            else if (transform.position.z > maxPosition.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, minPosition.z);
        }
    }
}
