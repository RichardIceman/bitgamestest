﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BitGamesTest
{


    public class Spot
    {
        //ключ это индекс в матрице
        public string key;

        public int i;
        public int j;

        //f(n) = g(n) + h(n)
        public int g;
        public float f;

        public bool isBlock;

        //откуда пришли
        public Spot prevSpot;

        public LinkedList<Spot> listNeighbors;

        private Vector2 size;

        public Spot(int _i, int _j, Vector2 _size)
        {
            key = _i.ToString()+_j.ToString();

            i = _i;
            j = _j;

            size = _size;

            g = int.MaxValue;
            f = int.MaxValue;
            isBlock = false;

            listNeighbors = new LinkedList<Spot>();
        }

        public Vector3 getCenter()
        {
            Vector3 center = Vector2.zero;

            center.x = size.x * j + size.x / 2;
            center.z = size.y * i + size.y / 2;
            center.y = 1;

            return center;
        }
    }

    public class AStar
    {
        private Spot[,] grid;
        private Vector2 sizeSpot;

        private int countRow;
        private int countColumn;

        public AStar(Vector2 _sizeGrid, int _countRow, int _countColumn)
        {
            countRow = _countRow;
            countColumn = _countColumn;

            sizeSpot.x = _sizeGrid.x / _countColumn;
            sizeSpot.y = _sizeGrid.y / _countRow;

            grid = new Spot[countRow, countColumn];

            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    grid[i, j] = new Spot(i, j, sizeSpot);

            setNeighbors();

        }

        public void UpdateGrid(Obstacle obstacle)
        {
            Vector2 size;
            size.x = obstacle.Radius * 2 + (obstacle.Position.x - obstacle.Radius) % sizeSpot.x;
            size.y = obstacle.Radius * 2 + (obstacle.Position.z - obstacle.Radius) % sizeSpot.y;

            int start_j = (int)((obstacle.Position.x - obstacle.Radius) / sizeSpot.x);
            int start_i = (int)((obstacle.Position.z - obstacle.Radius) / sizeSpot.y);

            float countBlockColumn = size.x / sizeSpot.x;
            float countBlockRow = size.y / sizeSpot.y;

            for (int i = start_i; i < start_i + countBlockRow && i < countRow; i++)
                for (int j = start_j; j < start_j + countBlockColumn && j < countColumn; j++)
                    grid[i, j].isBlock = true;

            setNeighbors();
        }

        public List<Vector3> FindPath(Vector3 start, Vector3 end)
        {
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                {
                    grid[i, j].f = float.MaxValue;
                    grid[i, j].g = int.MaxValue;
                    grid[i, j].prevSpot = null;
                }

            Dictionary<string, Spot> openSet = new Dictionary<string, Spot>();
            Dictionary<string, Spot> closedSet = new Dictionary<string, Spot>();

            int start_j = (int)(start.x / sizeSpot.x);
            int start_i = (int)(start.z / sizeSpot.y);

            //старт
            Spot startSpot = grid[start_i, start_j];
            Spot currentSpot = startSpot;

            //конечная ячейка
            start_j = (int)(end.x / sizeSpot.x);
            start_i = (int)(end.z / sizeSpot.y);

            Spot endSpot = grid[start_i, start_j];

            openSet.Add(currentSpot.key, currentSpot);

            currentSpot.g = 0;
            currentSpot.f = h(currentSpot, endSpot);

            //тело
            while (openSet.Count > 0)
            {
                currentSpot = findLowestF(openSet);
                if (currentSpot == endSpot) return getPath(startSpot, endSpot);

                openSet.Remove(currentSpot.key);
                closedSet.Add(currentSpot.key, currentSpot);

                foreach (Spot neighbor in currentSpot.listNeighbors)
                {
                    if (closedSet.ContainsKey(neighbor.key) || neighbor.isBlock) continue;

                    int tmpG = currentSpot.g + 1;

                    if (!openSet.ContainsKey(neighbor.key))
                    {
                        openSet.Add(neighbor.key, neighbor);
                    }

                    if (tmpG < neighbor.g)
                    {
                        neighbor.prevSpot = currentSpot;
                        neighbor.g = tmpG;
                        neighbor.f = tmpG + h(neighbor, endSpot);
                    }
                }
            }
            //пути нет
            return null;
        }

        private float h(Spot spot, Spot end)
        {
            int dI = end.i - spot.i;
            int dJ = end.j - spot.j;

            return Mathf.Sqrt(dI * dI + dJ * dJ);
        }

        private List<Vector3> getPath(Spot start, Spot end)
        {
            List<Spot> pathSpot = new List<Spot>();

            while (end.prevSpot != start)
            {
                pathSpot.Insert(0, end);
                end = end.prevSpot;
                
                if (pathSpot.Count > 2)
                {
                    int prev_dI = pathSpot[0].i - pathSpot[1].i;
                    int next_dI = pathSpot[1].i - pathSpot[2].i;

                    int prev_dJ = pathSpot[0].j - pathSpot[1].j;
                    int next_dJ = pathSpot[1].j - pathSpot[2].j;

                    //если 3 звена в пути идут в одном направлении, удаляем среднее из них
                    if ((prev_dI == next_dI || prev_dI < 0 && next_dI < 0 || prev_dI > 0 && next_dI > 0) &&
                        (prev_dJ == next_dJ || prev_dJ < 0 && next_dJ < 0 || prev_dJ > 0 && next_dJ > 0))
                        pathSpot.RemoveAt(1);
                }
            }

            List<Vector3> path = new List<Vector3>();

            for (int i = 0; i < pathSpot.Count; i++)
                path.Add(pathSpot[i].getCenter());

            return path;
        }

        private void setNeighbors()
        {
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                {
                    grid[i, j].listNeighbors.Clear();

                    if (i > 0)               grid[i, j].listNeighbors.AddLast(grid[i - 1, j]);
                    if (i < countRow - 1)    grid[i, j].listNeighbors.AddLast(grid[i + 1, j]);
                    if (j > 0)               grid[i, j].listNeighbors.AddLast(grid[i, j - 1]);
                    if (j < countColumn - 1) grid[i, j].listNeighbors.AddLast(grid[i, j + 1]);

                    //диагонали
                    if (i > 0            && j > 0               && !grid[i - 1, j].isBlock && !grid[i, j - 1].isBlock) grid[i, j].listNeighbors.AddLast(grid[i - 1, j - 1]);
                    if (i > 0            && j < countColumn - 1 && !grid[i - 1, j].isBlock && !grid[i, j + 1].isBlock) grid[i, j].listNeighbors.AddLast(grid[i - 1, j + 1]);
                    if (i < countRow - 1 && j > 0               && !grid[i + 1, j].isBlock && !grid[i, j - 1].isBlock) grid[i, j].listNeighbors.AddLast(grid[i + 1, j - 1]);
                    if (i < countRow - 1 && j < countColumn - 1 && !grid[i + 1, j].isBlock && !grid[i, j + 1].isBlock) grid[i, j].listNeighbors.AddLast(grid[i + 1, j + 1]);
                }
        }

        private Spot findLowestF(Dictionary<string, Spot> openSet)
        {
            Spot lowest = null;

            foreach (string key in openSet.Keys)
            {
                if (lowest == null) lowest = openSet[key];
                else if (openSet[key].f < lowest.f) lowest = openSet[key];
            }

            return lowest;
        }
    }
}
